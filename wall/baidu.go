package wall

import (
	"crypto/md5"
	"encoding/hex"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func GetBaiduImageURL(word string) (imgURL string, imgFilename string) {
	defer SetRandomWall()
	
	log.Println("本次百度壁纸的搜索关键词:" + word)

	indexUrl := "https://image.baidu.com/"
	response, err := http.Get(indexUrl)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	cookies := response.Cookies()
	cookieStr := ""
	for i := 0; i < len(cookies); i++ {
		cookieStr += cookies[i].Name + "=" + cookies[i].Value + "; "
	}
	cookieStr = strings.TrimRight(cookieStr, "; ")

	if word == "" {
		word = "壁纸"
	} else {
		word = "壁纸" + word
	}

	searchUrl := "https://image.baidu.com/search/index?tn=resulttagjson&word="
	searchUrl += url.QueryEscape(word)
	searchUrl = strings.Replace(searchUrl, "+", "%20", -1)
	width := GetWinScreenSize(0)
	if width != 0 {
		searchUrl += "&width=" + strconv.Itoa(width)
	}
	height := GetWinScreenSize(1)
	if height != 0 {
		searchUrl += "&height=" + strconv.Itoa(height)
	}

	request, err := http.NewRequest("GET", searchUrl, nil)
	if err != nil {
		panic(err)
	}
	request.Header.Set("Cookie", cookieStr)
	request.Header.Set("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:98.0) Gecko/20100101 Firefox/98.0")
	client := &http.Client{}
	response, err = client.Do(request)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()

	re := regexp.MustCompile(`{"ObjURL":".+?",`)
	htmlData, _ := ioutil.ReadAll(response.Body)
	objectUrl := re.FindAllString(string(htmlData), -1)
	if len(objectUrl) <= 0 {
		panic("未找到搜索关键词: " + word + " 的百度壁纸图片")
	}

	rand.Seed(time.Now().Unix())
	randIndex := rand.Intn(len(objectUrl))

	imgURL = objectUrl[randIndex]
	imgURL = imgURL[11 : len(imgURL)-2]
	imgURL = strings.Replace(imgURL,`\`,"",-1)
	log.Println("本次随机的百度壁纸图片URL:" + imgURL)

	//imgExt := imgURL[strings.LastIndexAny(imgURL, "."):]
	imgExt := ".jpg"

	h := md5.New()
	h.Write([]byte(imgURL))
	cipherStr := h.Sum(nil)
	imgFilename = hex.EncodeToString(cipherStr)

	return imgURL, "baidu/" + imgFilename + imgExt
}
